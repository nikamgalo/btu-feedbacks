app.controller('MainController', function($scope, $http) {
    $scope.submitForm = function() {

      if ($scope.Form.$valid) {
        let subject = $scope.user.subject
        let text = $scope.user.text
        let captcha = $scope.user.captcha


        $http({
		  method: 'POST',
		  data: {
		  	'subject' : subject,
		  	'text' : text,
		  	'captcha' : captcha
		  },
		  // data: "?subject=" + subject + "&text=" + text + "&captcha=" + captcha,
		  url: base_url + 'manage/submit.php'
		}).then(function successCallback(response) {
			$scope.isSuccessVisible = true;
		}, function errorCallback(response) {
			$scope.isErrorVisible = true;
		});

      }

    };
})