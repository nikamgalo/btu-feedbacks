var app = angular.module('app', ['ngRoute']);


app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when('/', {
    	templateUrl: 'pages/home.html',
    	controller : 'MainController'
    })
    $locationProvider.html5Mode(true)
})

