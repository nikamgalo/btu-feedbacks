<?php if (count($errors) > 0): ?>
<div class="error_box txt_regular"><?php foreach ($errors as $error): ?><?= $error ?><?php endforeach ?> <span onclick="$('.error_box').hide();" class="modal_hider">X</span></div>
<?php endif; ?>
<?php if (count($success) > 0): ?>
<div class="success_box txt_regular"><?php foreach ($success as $success_text): ?><?= $success_text ?><?php endforeach ?> <span onclick="$('.success_box').hide();" class="modal_hider">X</span></div>
<?php endif; ?>