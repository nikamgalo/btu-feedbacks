<?php

session_start();

$errors = array();
$success = array();

error_reporting(0);



if (isset($_POST["subject"]) && isset($_POST["text"]) && isset($_POST["captcha_value"])) {

	if (empty($_POST["subject"]) || empty($_POST["text"]) || empty($_POST["captcha_value"])) {
		array_push($errors, "გთხოვთ შეავსოთ ყველა მოცემული ველი");
	}



	if ($_POST["captcha_value"] != $_SESSION["CaptchaCode"]) {
		array_push($errors, "დამცავი კოდის არასწორია");
	} else {

		if ($_FILES['feadback_file']['tmp_name'] == "") {
			$file_name = "";
		} else {

			if ($_FILES["feadback_file"]["size"] > 500000) {
			    array_push($errors, "ფაილის ზომა ზღვარს მიღმაა");
			} else {
				$fileinfo = PATHINFO($_FILES["feadback_file"]["name"]);
				$newFilename = time() . rand(1000, 99999) . "." . $fileinfo['extension'];
				move_uploaded_file($_FILES["feadback_file"]["tmp_name"], "manage/uploads/".$newFilename);
				$file_name = "manage/uploads/" . $newFilename;
			}  
		}


		if (count($errors) == 0) {

			include 'db.conn.php';
			$subject = $_POST['subject'];
			$feed_text = $_POST['text'];
			$sql = mysqli_query($conn, "INSERT INTO feedbacks (subject, feed_text, attached_file) VALUES ('$subject', '$feed_text', '$file_name') ");
			array_push($success, "წერილი წარმატებით გაიგზავნა");
		}

		session_unset();
		session_destroy();

	}

}


