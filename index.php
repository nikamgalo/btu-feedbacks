<?php include 'manage/submit.php';  ?>	
<!DOCTYPE html>
<html ng-app="app">
<head>
	<title>BTU feedback</title>
	<base href="<?= $_SERVER['REQUEST_URI'] ?>">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="author" content="Nika Mgaloblishvili"/>
	<link rel="apple-touch-icon" sizes="180x180" href="https://classroom.btu.edu.ge/res/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://classroom.btu.edu.ge/res/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://classroom.btu.edu.ge/res/img/favicon-16x16.png">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="res/css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular-route.js"></script>

	<script type="text/javascript">
		base_url = '<?= $_SERVER['REQUEST_URI'] ?>'
	</script>
	
</head>
<body>
<div id="particles-js"></div>
<div id="page">
	<div ng-include="'pages/header.html'"></div>
	<div class="login-box">
		<div ng-view></div>
	</div>
</div>

<?php include 'manage/errors.php'; ?>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="res/js/common.js?v=14"></script>
<script src="res/js/MainController.js?v=14"></script>
<script src="res/js/particles.js?v=14"></script>
</body>
</html>